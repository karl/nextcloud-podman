#!/usr/bin/env sh

# General
TZ="America/Sao_Paulo"
PODNAME="nextcloud"
DIR="$(pwd)"
EXPOSED_PORT="8080"

# Images
NEXTCLOUD_IMAGE="localhost/${PODNAME}-fpm"
WEB_IMAGE="localhost/${PODNAME}-nginx"
POSTGRES_IMAGE="docker.io/library/postgres:13-alpine"
REDIS_IMAGE="docker.io/library/redis:6-alpine"
BORGMATIC_IMAGE="docker.io/b3vis/borgmatic"

# Colors
RED="\e[31m\e[1m"
ORANGE="\e[38;5;214m"
BLUE="\e[94m"
RESET="\e[0m"

is_pod_running() {
  podman pod exists nextcloud
}

is_nginx_built() {
  podman image exists localhost/${PODNAME}-nginx
}

is_nextcloud_built() {
  podman image exists localhost/${PODNAME}-fpm
}

build_nginx() {
  printf "${BLUE}Building Nextcloud Web image${RESET}\n\n"
  podman build -t ${PODNAME}-nginx ${DIR}/nginx
}

build_nextcloud_fpm() {
  printf "${BLUE}Building Nextcloud FPM image${RESET}\n\n"
  podman build -t ${PODNAME}-fpm ${DIR}/nextcloud
}

create_pod() {
  printf "${BLUE}Creating Nextcloud Pod${RESET}\n"
  podman pod create --name ${PODNAME} -p ${EXPOSED_PORT}:80
}

remove_pod() {
  printf "${BLUE}Removing Nextcloud Pod${RESET}\n"
  podman pod stop ${PODNAME}
  podman pod rm ${PODNAME}
}

run_nextcloud_fpm() {
  printf "${BLUE}Starting Nextcloud FPM container${RESET}\n"
  podman run -d --name=${PODNAME}-fpm --pod ${PODNAME} \
    --restart=always \
    --env-file=${DIR}/nextcloud.env \
    -e TZ=${TZ} \
    -v ${PODNAME}:/var/www/html:z \
    -v ${PODNAME}-config:/var/www/html/config:z \
    -v ${PODNAME}-data:/var/www/html/data:z \
    ${NEXTCLOUD_IMAGE}
}

run_nextcloud_cron() {
  printf "${BLUE}Starting Nextcloud Cron container${RESET}\n"
  podman run -d --name=${PODNAME}-cron --pod ${PODNAME} \
    --restart=always \
    -e TZ=${TZ} \
    -v ${PODNAME}:/var/www/html:z \
    -v ${PODNAME}-config:/var/www/html/config:z \
    -v ${PODNAME}-data:/var/www/html/data:z \
    --entrypoint /cron.sh \
    ${NEXTCLOUD_IMAGE}
}

run_nginx() {
  printf "${BLUE}Starting Nginx container${RESET}\n"
  podman run -d --name=${PODNAME}-nginx --pod ${PODNAME} \
    --restart=always \
    -e TZ=${TZ} \
    -v ${PODNAME}:/var/www/html:z \
    ${WEB_IMAGE}
}

run_postgres() {
  printf "${BLUE}Starting Postgres container${RESET}\n"
  podman run -d --name ${PODNAME}-postgres --pod ${PODNAME} \
    --restart=always \
    --env-file=${DIR}/postgres.env \
    -e TZ=${TZ} \
    -v ${PODNAME}-postgres:/var/lib/postgresql/data:Z \
    ${POSTGRES_IMAGE}
}

run_redis() {
  printf "${BLUE}Starting Redis container${RESET}\n"
  podman run -d --name ${PODNAME}-redis --pod ${PODNAME} \
    -e TZ=${TZ} \
    --restart=always \
    ${REDIS_IMAGE}
}

run_borgmatic() {
  printf "${BLUE}Starting Borgmatic container${RESET}\n"
  podman run -d --name ${PODNAME}-backup --pod ${PODNAME} \
    --restart=always \
    --env-file=${DIR}/borgmatic.env \
    -v ${PODNAME}:/mnt/source/base:z \
    -v ${PODNAME}-config:/mnt/source/config:z \
    -v ${PODNAME}-data:/mnt/source/data:z \
    -v ${DIR}/borgmatic:/etc/borgmatic.d:z \
    -v ${PODNAME}-borg:/root/.config/borg:Z \
    -v ${PODNAME}-borg-cache:/root/.cache/borg:Z \
    -v ${DIR}/ssh:/root/.ssh:z \
    ${BORGMATIC_IMAGE}
}

init() {
  is_pod_running && remove_pod
  create_pod

  run_postgres
  run_redis

  if is_nextcloud_built; then
    run_nextcloud_fpm
  else
    build_nextcloud_fpm
    run_nextcloud_fpm
  fi

  if is_nginx_built; then
    run_nginx
  else
    build_nginx
    run_nginx
  fi

  run_borgmatic
}

update() {
  build_nginx
  build_nextcloud_fpm

  printf "${BLUE}Pulling Postgres${RESET}\n"
  podman pull ${POSTGRES_IMAGE}

  printf "${BLUE}Pulling Redis${RESET}\n"
  podman pull ${REDIS_IMAGE}

  printf "${BLUE}Pulling Borgmatic${RESET}\n"
  podman pull ${BORGMATIC_IMAGE}
}

maintenance_mode() {
  podman exec --user www-data ${PODNAME}-fpm php occ maintenance:mode --$1
}

backup() {
  if ! is_pod_running; then
    printf "${BLUE}Starting Nextcloud${RESET}\n"
    init
  fi

  maintenance_mode on

  remove_pod
  podman pod create --name ${PODNAME}

  run_postgres
  sleep 10
  run_borgmatic

  printf "${BLUE}Creating Borgmatic repository if not created${RESET}\n"
  podman exec -it ${PODNAME}-backup borgmatic --init --encryption repokey-blake2
  printf "${BLUE}Creating Borgmatic archive${RESET}\n"
  podman exec ${PODNAME}-backup borgmatic create --progress --files -v2

  printf "${BLUE}Starting Nextcloud${RESET}\n"
  init
  maintenance_mode off
}

restore() {
  if ! is_pod_running; then
    printf "${BLUE}Starting Nextcloud${RESET}\n"
    init
  fi

  maintenance_mode on

  remove_pod
  podman pod create --name ${PODNAME}

  run_postgres
  sleep 10
  run_borgmatic

  printf "${BLUE}Restoring database${RESET}\n"
  podman exec ${PODNAME}-backup borgmatic restore --archive latest
  printf "${BLUE}Restoring files${RESET}\n"
  podman exec ${PODNAME}-backup borgmatic extract --archive latest

  printf "${BLUE}Starting Nextcloud${RESET}\n"
  init
  maintenance_mode off
}

case $1 in
  init)
    init
    exit 1
    ;;
  update)
    update
    exit 1
    ;;
  backup)
    backup
    exit 1
    ;;
  restore)
    restore
    exit 1
    ;;
  *)
    printf "${RED}Error: ${ORANGE}Unrecognized command.${RESET}"
    exit 1
    ;;
esac